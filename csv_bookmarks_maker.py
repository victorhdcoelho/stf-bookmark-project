import sys
import os
import json
import pandas as pd
from getbookmarks.bookmark import GetBookMarks
import threading
import queue
import logging

tup_result = []
total_count = 0

with open("exportadosComAndamento8249.json") as f:
    JSON = json.load(f)

PROCESS = ["{}_{}".format(each["sig_classe_proces"], each["num_processo"])
           for each in JSON["items"]]
LENGHT = len(PROCESS)
COUNT = 0
ERRORS = []


class PathThreadBookMark(threading.Thread):
    def __init__(self, queue, name):
        threading.Thread.__init__(self)
        self.queue = queue
        self.name = name

    def parallel_get_bookmark(self, file, path):
        global total_count
        global PROCESS
        global COUNT
        file_buffer = open(path, "rb")
        try:
            get_b = GetBookMarks(file_buffer, file)
        except Exception as erro:
            logging.error("Error reader. Process: {} Error: {}".format(
                file,
                erro
            ))
            ERRORS.append(file)
        result = get_b.get_bookmarks()
        file_buffer.close()
        total_count += 1
        message = "ATUAL: {}\t THR: {}\t FILE: {} PROCESS {} PORCENTAGEM {}"
        logging.info(message.format(total_count,
                                    self.name,
                                    file,
                                    COUNT,
                                    round((COUNT/LENGHT)*100, 2)))
        return (file, result)

    def return_name_and_bookmarks_with_pg(self, path):
        global tup_result
        global PROCESS
        global COUNT
        for process in os.listdir(path):
            if process in PROCESS:
                logging.info("START PROCESS: {}".format(process))
                COUNT += 1
                path_process = os.path.join(path, process)
                for file in os.listdir(path_process):
                    path_file = os.path.join(path_process, file)
                    tup_result.append(self.parallel_get_bookmark(
                        file,
                        path_file
                    ))
            else:
                logging.info("SKIP PROCESS: {}".format(process))
                continue

    def run(self):
        while True:
            path = self.queue.get()
            self.return_name_and_bookmarks_with_pg(path)
            self.queue.task_done()


def create_df_to_bookmarks(list_file_name, list_result):
    logging.info("START CREATE DATAFRAME")
    dict_pandas = {"ID": [],
                   "BOOKMARKS": []}
    for file, result in zip(list_file_name, list_result):
        if result != []:
            dict_pandas["ID"].append(file)
            dict_pandas["BOOKMARKS"].append(result)
        else:
            continue

    logging.info("END CREATE DATAFRAME")
    return pd.DataFrame(data=dict_pandas)


def create_csv_to_bookmarks(dataframe):
    logging.info("START MAKE CSV")
    dataframe.to_csv("./bookmarks.csv", sep="@")
    logging.info("START WRITE FILE ERRORS")
    with open("errors.txt", "w") as f:
        for each in ERRORS:
            f.write(each)
            f.write("\n")
    logging.info("DONE")


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    path = sys.argv[1]
    logging.info("Absolute path: {}".format(path))
    pathqueue = queue.Queue()
    themes = os.listdir(path)
    for name in range(100):
        thr = PathThreadBookMark(pathqueue, name)
        thr.setDaemon(True)
        thr.start()

    for theme in themes:
        logging.info("START THEME: {}".format(theme))
        pathqueue.put(os.path.join(path, theme))

    pathqueue.join()
    idx = [id for id, val in tup_result]
    vals = [val for id, val in tup_result]
    create_csv_to_bookmarks(create_df_to_bookmarks(idx, vals))
